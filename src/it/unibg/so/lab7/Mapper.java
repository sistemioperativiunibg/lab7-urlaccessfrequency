package it.unibg.so.lab7;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Mapper implements Runnable {
	
	private CyclicBarrier barrier;	
	private List<String> urls;
	Map<String, Integer> counts;
	
	public Mapper(List<String> urls, CyclicBarrier barrier){
		this.urls = urls;
		this.barrier = barrier;
		this.counts = new HashMap<String, Integer>();
	}

	@Override
	public void run() {
		for(String url: urls){
			if(!counts.containsKey(url))
				counts.put(url, 1);
			else
				counts.put(url, counts.get(url) + 1);
		}
		
		try {
			barrier.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
	}

}
