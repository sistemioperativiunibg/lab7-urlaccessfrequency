package it.unibg.so.lab7;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CyclicBarrier;

public class Driver {
	
	public static final String FILE_NAME = "src/it/unibg/so/lab7/resources/es2.txt";
	public static final int THREADS = 10;

	public static void main(String[] args) {
		List<Mapper> mappers = new ArrayList<Mapper>();
		CyclicBarrier barrier = new CyclicBarrier(THREADS, new Reducer(mappers));
		List<String> lines = null;
		try {
			lines = readLines(new FileReader(FILE_NAME));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for (int i=0; i<THREADS; ++i) {
			Mapper m = new Mapper(lines.subList(i*20, (i*20)+20), barrier);
            mappers.add(m);
            new Thread(m).start();
		}

	}
	
	public static List<String> readLines(Reader r){
		List<String> lines = new ArrayList<String>();
		Scanner s = new Scanner(r);
		while(s.hasNext())
			lines.add(s.next());
		s.close();
		return lines;		
	}

}
