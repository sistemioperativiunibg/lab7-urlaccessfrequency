package it.unibg.so.lab7;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Reducer implements Runnable {
	
	private Map<String, Integer> finalCounts;
	private List<Mapper> mappers;
	
	public Reducer(List<Mapper> mappers){
		this.mappers = mappers;
		finalCounts = new HashMap<String, Integer>();
	}
	

	@Override
	public void run() {
		for(Mapper m: mappers){
			for(String url: m.counts.keySet()){
				if(!finalCounts.containsKey(url))
					finalCounts.put(url, m.counts.get(url));
				else
					finalCounts.put(url, finalCounts.get(url) + m.counts.get(url));
			}
		}
		
		for(String url: finalCounts.keySet()){
			System.out.println("URL: " + url + " COUNTS: " + finalCounts.get(url));
		}
	}

}
