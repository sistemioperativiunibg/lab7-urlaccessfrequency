# URL ACCESS FREQUENCY EXAMPLE

Implementare un programma *Java* che, dato in input un *file di log* che contiene un insieme di richieste per pagine web, ad esempio, 
```
#!
http://url-1
http://url-2
http://url-3
http://url-6
http://url-7
http://url-8
http://url-9
http://url-1
http://url-2
http://url-3
```
restituisca su **stdout** un insieme di coppie
```
#!
<URL, total_count>
```
ovvero per ogni `URL`, determinare il numero totale di richieste effettuate (`total_count`).

Il conteggio deve avvenire in modo parallelo suddividendo il carico computazionale su **N** thread *Worker*.
Ogni *Worker* produce in output i risultati parziali relativi al conteggio delle `URL` presenti in una porzione dell’input totale.

Dopodiché una procedura di **Merge** dei risultati parziali si occupa di sommare il valore `total_count` per medesimi `URL`.